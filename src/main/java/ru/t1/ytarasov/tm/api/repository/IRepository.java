package ru.t1.ytarasov.tm.api.repository;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    Integer getSize();

    M add(M model);

    boolean existsById(String id);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    void clear();

    M remove(M model) throws AbstractException;

    M removeById(String id) throws AbstractException;

    M removeByIndex(Integer index) throws AbstractException;

}
