package ru.t1.ytarasov.tm.api.repository;

import ru.t1.ytarasov.tm.api.service.IUserOwnedService;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, Status status);

    Project create(String userId, String name, String description, Status status);

}
