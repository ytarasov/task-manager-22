package ru.t1.ytarasov.tm.command.user;

import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    public static final String NAME = "remove-user";

    public static final String DESCRIPTION = "Remove user";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }
}
