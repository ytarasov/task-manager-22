package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = "./commands.xml";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = "./errors.xml";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "./messages.xml";

    private static final LogManager MANAGER = LogManager.getLogManager();

    private static final Logger LOGGER_ROOT = getLoggerRoot();

    private static final Logger LOGGER_COMMANDS = getLoggerCommands();

    private static final Logger LOGGER_ERRORS = getLoggerErrors();

    private static final Logger LOGGER_MESSAGES = getLoggerMessages();

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(LOGGER_COMMANDS, COMMANDS_FILE, false);
        registry(LOGGER_ERRORS, ERRORS_FILE, true);
        registry(LOGGER_MESSAGES, MESSAGES_FILE, true);
    }

    private static Logger getLoggerRoot() {
        return Logger.getLogger("");
    }

    private static Logger getLoggerCommands() {
        return Logger.getLogger(COMMANDS);
    }

    private static Logger getLoggerErrors() {
        return Logger.getLogger(ERRORS);
    }

    private static Logger getLoggerMessages() {
        return Logger.getLogger(MESSAGES);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage();
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGES.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMANDS.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        LOGGER_ERRORS.log(Level.SEVERE, e.getMessage(), e);
    }

}
